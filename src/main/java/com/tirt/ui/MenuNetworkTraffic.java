package com.tirt.ui;

import com.tirt.network.EthernetFrame;
import com.tirt.network.IpPacket;
import com.tirt.utils.ActionTakenListener;
import com.tirt.utils.EventType;

import java.util.Map;

/**
 * Created by Łukasz on 23.05.2017.
 */
public class MenuNetworkTraffic extends Menu implements MenuElement, ActionTakenListener {
    private static final MenuNetworkTraffic instance = new MenuNetworkTraffic();

    private boolean showTraffic = false;
    private MenuConsole output = createConsole();

    private MenuNetworkTraffic() {}

    @Override
    public void onActionTaken(EventType eventType, Map<String, Object> parameters) {
        if(showTraffic) {
            synchronized (this) {
                String message = createMessage(eventType, parameters);
                output.println(message);
            }
        }
    }

    private String createMessage(EventType eventType, Map<String, Object> parameters) {
        EthernetFrame frame = (EthernetFrame) parameters.get("frame");
        IpPacket packet = (IpPacket) parameters.get("packet");
        String ipMessage = (String) parameters.get("message");
        
        StringBuilder builder = new StringBuilder();
        String message = "";
        builder.append(message);
        if (eventType.equals(EventType.FRAME_SENT)) {
            builder.append("Przekierowano ramkę: ");
            builder.append(createFrameProps(frame));
        } else if (eventType.equals(EventType.FRAME_RECEIVED)) {
            builder.append("Odebrano ramkę: ");
            builder.append(createFrameProps(frame));
        } else if (eventType.equals(EventType.FRAME_DROPPED)) {
            builder.append("Zdropowano ramkę: ");
            builder.append(createFrameProps(frame));
        } else if (eventType.equals(EventType.FRAME_SENT_NEW)) {
            builder.append("Stworzenie ramki: ");
            builder.append(createFrameProps(frame));
        } else if (eventType.equals(EventType.PACKET_ASSEMBLED)) {
            builder.append("Dostarczono pakiet: ");
            builder.append(createPacketProps(packet));
            builder.append(", Wiadomość: ");
            builder.append(ipMessage);
        }
        return builder.toString();
    }

    private String createPacketProps(IpPacket packet) {
        if(packet != null) {
            String dest = packet.destination.toString();
            String source = packet.source.toString();
            StringBuilder builder = new StringBuilder();
            builder.append("[Sender = ");
            builder.append(source);
            builder.append("] [Recipient = ");
            builder.append(dest);
            builder.append("]");
            return builder.toString();
        }
        return "";
    }

    private String createFrameProps(EthernetFrame frame) {
        if(frame != null) {
            String sender = frame.getSender() != null ? frame.getSender().getStringAddress() : "";
            String recipient = frame.getRecipient() != null ? frame.getRecipient().getStringAddress() : "";
            StringBuilder builder = new StringBuilder();
            builder.append("Sender [");
            builder.append(sender);
            builder.append("], ");
            builder.append("Recipient [");
            builder.append(recipient);
            builder.append("]");
            return builder.toString();
        }
        return "";
    }

    @Override
    public String elementName() {
        return "Podgląd ruchu w sieci";
    }

    @Override
    public void start() {
        output.showConsole();
        showTraffic = true;
        //System.out.println("Naciśnij ENTER aby przerwać");
        //super.nextLine();
        //showTraffic = false;
        //output.closeConsole();
        super.start();
    }

    public static MenuNetworkTraffic getInstance() {
        return instance;
    }
}
