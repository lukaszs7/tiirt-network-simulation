package com.tirt.ui;

/**
 * Created by Łukasz on 23.05.2017.
 */
public interface MenuElement {
    String elementName();
    void start();
}
