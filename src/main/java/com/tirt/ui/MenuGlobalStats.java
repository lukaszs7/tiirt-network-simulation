package com.tirt.ui;

import com.tirt.utils.StatsManager;

/**
 * Created by Łukasz on 23.05.2017.
 */
public class MenuGlobalStats extends Menu implements MenuElement {
    private StatsManager statsManager = StatsManager.getInstance();

    public MenuGlobalStats() {
    }

    private String createStatsTable(StatsManager.Statistics stats) {
        StringBuilder builder = new StringBuilder();
        String header = createStatsHeader();
        String row = createStatsRow(stats.internalStatistics, stats.outgoingStatistics);
        String close = closeTable();
        return builder.append(header).append(row).append(close).toString();
    }

    private String createStatsHeader() {
        return  "+-----------------------------------------------------+-----------------------------------------------------+\n" +
                "| Internal                                            |  OutgoingPackets                                    |\n" +
                "+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+\n" +
                "| SENT            | RECEIVED        | DROP            | SENT            | RECEIVED        | DROP            |\n" +
                "+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+\n";
    }

    private String createStatsRow(StatsManager.InternalStatistics internal, StatsManager.OutgoingPacketsStatistics outgoing) {
        return String.format("|   %-11s   |   %-11s   |   %-11s   |   %-11s   |   %-11s   |   %-11s   |%n",
                internal.sent, internal.received, internal.dropped, outgoing.sent, outgoing.received, outgoing.dropped);
    }

    private String closeTable() {
        return "+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+\n";
    }

    @Override
    public String elementName() {
        return "Statystyki globalne";
    }

    @Override
    public void start() {
        StatsManager.Statistics stats = statsManager.getGlobalStatistic();
        String table = createStatsTable(stats);
        System.out.println("Statystyki globalne:");
        System.out.println(table);
        super.start();
    }
}
