package com.tirt.ui;

import com.tirt.network.Device;

import java.util.*;

import org.graphstream.graph.*;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;

/**
 * Created by Łukasz on 23.05.2017.
 */
public class MenuTopology extends Menu implements MenuElement {
    private static final String STYLES = "node {\n" +
            "    size: 35px;\n" +
            "}\n" +
            "\n" +
            "node.router {\n" +
            "    fill-color: #ff7d72;\n" +
            "}\n" +
            "\n" +
            "node.switch {\n" +
            "    fill-color: #feff8b;\n" +
            "}\n" +
            "\n" +
            "node.host {\n" +
            "    fill-color: #6bc7ff;\n" +
            "}";

    private List<Device> devices;
    private Map<Device, String> mapDevices;
    private Graph graph;

    public MenuTopology(List<Device> devices) {
        this.devices = devices;
        this.mapDevices = createMapDevices();
        graph = new SingleGraph("Topologia sieci");
    }

    private Map<Device, String> createMapDevices() {
        Map<Device, String> map = new HashMap<>();
        int hostCount = 1;
        int switchCount = 1;
        int routerCount = 1;

        for (int i = 0; i < devices.size(); i++) {
            Device device = devices.get(i);
            String deviceName = super.deviceName(devices, device);
            map.put(device, deviceName);
        }
        return map;
    }

    private void createGraphNodes(Graph graph) {
        Set<Map.Entry<Device, String>> entries = mapDevices.entrySet();
        Iterator<Map.Entry<Device, String>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            Map.Entry<Device, String> next = iterator.next();
            String nextNode = next.getValue();
            if(!isNodeExists(nextNode)) {
                graph.addNode(nextNode);
                graph.getNode(nextNode).addAttribute("ui.label", nextNode);
            }
        }
    }

    private void createGraphConnections(Graph graph) {
        Set<Map.Entry<Device, String>> entries = mapDevices.entrySet();
        Iterator<Map.Entry<Device, String>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            Map.Entry<Device, String> next = iterator.next();
            String nextNode = next.getValue();
            Device nextDevice = next.getKey();
            List<Device> connected = nextDevice.getConnectedDevices();
            for (int i = 0; i < connected.size(); i++) {
                Device toConnect = connected.get(i);
                String edgeName = createEdgeName(nextDevice, toConnect);
                if(toConnect != null && !isEdgeExists(edgeName)) {
                    graph.addEdge(edgeName, nextNode, mapDevices.get(toConnect));
                }
            }
        }
    }

    private void createGraphStyles(Graph graph) {
        graph.addAttribute("ui.stylesheet", STYLES);

        Collection<Node> nodeSet = graph.getNodeSet();
        Iterator<Node> iterator = nodeSet.iterator();
        while (iterator.hasNext()) {
            Node nextNode = iterator.next();
            String nextNodeName = nextNode.getAttribute("ui.label");
            createProperStyle(nextNode, nextNodeName);
        }
    }

    private void createProperStyle(Node node, String nodeName) {
        String className = nodeName.substring(0, nodeName.indexOf("-")).toLowerCase();
        node.addAttribute("ui.class", className);
    }

    private String createEdgeName(Device nextDevice, Device toConnect) {
        return mapDevices.get(nextDevice)+"_"+mapDevices.get(toConnect);
    }

    private boolean isNodeExists(String nodeName) {
        return graph.getNode(nodeName) != null;
    }

    private boolean isEdgeExists(String nodeName) {
        String[] edges = nodeName.split("_");
        return graph.getEdge(nodeName) != null || graph.getEdge(edges[1]+"_"+edges[0]) != null;
    }

    private void showGraph(Graph graph) {
        createGraphNodes(graph);
        createGraphConnections(graph);
        createGraphStyles(graph);
        Viewer viewer = graph.display();
        viewer.setCloseFramePolicy(Viewer.CloseFramePolicy.HIDE_ONLY);
    }

    @Override
    public String elementName() {
        return "Inspekcja Topologii";
    }

    @Override
    public void start() {
        showGraph(graph);
        super.start();
    }
}
