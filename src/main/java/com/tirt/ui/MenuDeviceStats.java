package com.tirt.ui;

import com.tirt.network.Device;
import com.tirt.network.MacAddress;
import com.tirt.utils.StatsManager;

import java.util.List;

/**
 * Created by Łukasz on 23.05.2017.
 */
public class MenuDeviceStats extends Menu implements MenuElement {
    private StatsManager statsManager = StatsManager.getInstance();
    private List<Device> devices;

    public MenuDeviceStats(List<Device> devices) {
        this.devices = devices;
    }

    private String createStatsTable(StatsManager.Statistics stats) {
        StringBuilder builder = new StringBuilder();
        String header = createStatsHeader();
        String row = createStatsRow(stats.internalStatistics, stats.outgoingStatistics);
        String close = closeTable();
        return builder.append(header).append(row).append(close).toString();
    }

    private String createStatsHeader() {
        return  "+-----------------------------------------------------+-----------------------------------------------------+\n" +
                "| Internal                                            |  OutgoingPackets                                    |\n" +
                "+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+\n" +
                "| SENT            | RECEIVED        | DROP            | SENT            | RECEIVED        | DROP            |\n" +
                "+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+\n";
    }

    private String createStatsRow(StatsManager.InternalStatistics internal, StatsManager.OutgoingPacketsStatistics outgoing) {
        return String.format("|   %-11s   |   %-11s   |   %-11s   |   %-11s   |   %-11s   |   %-11s   |%n",
                internal.sent, internal.received, internal.dropped, outgoing.sent, outgoing.received, outgoing.dropped);
    }

    private String closeTable() {
        return "+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+\n";
    }

    private MacAddress deviceMacAddress(Device device) {
        for (int i = 0; i < device.getOwnMacs().size(); i++) {
            return device.getOwnMacs().get(i);
        }
        return null;
    }

    @Override
    public String elementName() {
        return "Statystyki dla urządzenia";
    }

    @Override
    public void start() {
        System.out.println("Statystyki dla urządzenia");
        for (int i = 0; i < devices.size(); i++) {
            System.out.println(deviceName(devices, devices.get(i)));
            MacAddress deviceMac = deviceMacAddress(devices.get(i));
            String table = createStatsTable(statsManager.getStatisticByMac(deviceMac));
            System.out.println(table);
        }
        super.start();
    }
}
