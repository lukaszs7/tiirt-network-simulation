package com.tirt.ui;

import com.tirt.network.Device;
import com.tirt.network.MacAddress;

import java.util.*;

/**
 * Created by Łukasz on 23.05.2017.
 */
public class MenuMACTable extends Menu implements MenuElement {
    private List<Device> devices;

    public MenuMACTable(List<Device> devices) {
        this.devices = devices;
    }

    private String createMACTable(Device device) {
        String header = createMACTableHeader();
        StringBuilder builder = new StringBuilder();
        builder.append(header);
        Set<Map.Entry<MacAddress, Integer>> entrySet = device.getMacTable().entrySet();
        Iterator it = entrySet.iterator();
        int ordinal = 1;
        while (it.hasNext()) {
            Map.Entry<MacAddress, Integer> next = (Map.Entry<MacAddress, Integer>) it.next();
            MacAddress key = next.getKey();
            int value = next.getValue();
            String row = createMACTableRow(ordinal++, key, value);
            builder.append(row);
        }
        return builder.append(closeTable()).toString();
    }

    private String createMACTableHeader() {
        return  "+-----------------------+-----------+\n"+
                "| Adres MAC             | Wartość   |\n"+
                "+-----------------------+-----------+\n";
    }

    private String createMACTableRow(int ordinal, MacAddress key, int value) {
        return String.format("|   %-17s   |  %-7d  |%n", key.getStringAddress(), value);
    }

    private String closeTable() {
        return "+-----------------------+-----------+\n";
    }

    @Override
    public String elementName() {
        return "Podgląd tablic MAC";
    }

    @Override
    public void start() {
        for (int i = 0; i < devices.size(); i++) {
            System.out.println(deviceName(devices, devices.get(i))+":");
            String macTable = createMACTable(devices.get(i));
            System.out.println(macTable);
        }
        super.start();
    }
}
