package com.tirt.ui;

import com.tirt.network.*;
import com.tirt.utils.ActionTakenListener;

import java.util.*;

/**
 * Created by Łukasz on 23.05.2017.
 */
public class Menu implements Runnable {
    protected static final int MENU_INDEX_START = 1;
    protected static final char MAIN_MENU_CHARACTER = '!';

    private static List<MenuElement> menuElements;

    private Network network;
    private Scanner scanner = new Scanner(System.in);

    public Menu() {}

    public Menu(Network network) {
        this.network = network;
        menuElements = new ArrayList<>();
        createMenuElements();
    }

    public void start() {
        printMainMenu();
        int input = nextChooseInput(MENU_INDEX_START, menuElements.size());
        startMenuElement(input);
    }

    private void startMenuElement(int input) {
        menuElements.get(input - 1).start();
    }

    protected int nextChooseInput(int startIndex, int endIndex) {
        int number = 0;
        do{
            System.out.println("\nWybierz pozycję, zakres:["+startIndex+";"+endIndex+"]:");
            try {
                number = scanner.nextInt();
            } catch (InputMismatchException ime) {
                System.out.println("Podano zły format wejściowy");
                scanner.next();
            }
        } while(number < startIndex || number > endIndex);
        return number;
    }

    protected int nextIntegerInput() {
        try {
            return scanner.nextInt();
        } catch (InputMismatchException ime) {
            System.out.println("Podano zły format wejściowy");
            scanner.next();
        }
        return 0;
    }

    protected String deviceName(List<Device> devices, Device device) {
        int nextDeviceNumber = deviceNumber(devices, device);

        if(device instanceof Router) {
            return "Router-"+nextDeviceNumber;
        } else if (device instanceof Switch) {
            return "Switch-"+nextDeviceNumber;
        } else if (device instanceof Host || device instanceof IpHost) {
            return "Host-"+nextDeviceNumber;
        } else {
            return "";
        }
    }

    protected Device getDevice(List<Device> devices, String deviceId) {
        for (int i = 0; i < devices.size(); i++) {
            if(deviceName(devices, devices.get(i)).equals(deviceId)) {
                return devices.get(i);
            }
        }
        return null;
    }

    protected boolean isHost(Device device) {
        return device.getClass().getName().replace("com.tirt.network.","").equals("IpHost");
    }

    protected boolean isSwitch(Device device) {
        return device.getClass().getName().replace("com.tirt.network.","").equals("Switch");
    }

    protected boolean isRouter(Device device) {
        return device.getClass().getName().replace("com.tirt.network.","").equals("Router");
    }

    protected boolean hasInterval(Device device) {
        return (isHost(device) || isRouter(device));
    }

    protected String nextStringInput() {
        return scanner.next();
    }

    protected String nextLine() {
        scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    protected String nextLineWithBackInfo(String originalInfo) {
        System.out.println(originalInfo+" (Powrót - '"+ MAIN_MENU_CHARACTER +"'):");
        return nextStringInput();
    }

    protected int nextIntegerWithBackInfo(String originalInfo, int min, int max) {
        String strInput = nextLineWithBackInfo(originalInfo);
        if(isBackwards(strInput)) {
            return -1;
        }

        int input = convertToInteger(strInput, min, max);
        return input;
    }

    protected boolean isBackwards(String input) {
        return input.equals(MAIN_MENU_CHARACTER+"");
    }

    protected int convertToInteger(String strInput, int min, int max) {
        try {
            int convertedVal = Integer.valueOf(strInput);
            if(convertedVal > max || convertedVal < min) {
                System.out.println("Nie ma takiej pozycji");
                return -1;
            }
            return convertedVal;
        } catch (NumberFormatException e) {
            System.out.println("Podano zły format wejściowy");
        }
        return -1;
    }

    public void printMainMenu() {
        final int[] i = {MENU_INDEX_START};
        menuElements.stream().forEach(e -> System.out.println((i[0]++)+". "+e.elementName()));
    }

    protected MenuConsole createConsole() {
        MenuConsole menuConsole = new MenuConsole(MenuConsole.TITLE, MenuConsole.WIDTH, MenuConsole.HEIGHT);
        return menuConsole;
    }

    private int deviceNumber(List<Device> devices, Device device) {
        int routerNb = 0;
        int hostNb = 0;
        int switchNb = 0;
        for (int i = 0; i < devices.size(); i++) {
            Device next = devices.get(i);

            if(isHost(next))
                hostNb++;
            if(isSwitch(next))
                switchNb++;
            if(isRouter(next))
                routerNb++;

            if(device == next && isRouter(device))
                return routerNb;
            if(device == next && isSwitch(device))
                return switchNb;
            if(device == next && isHost(device))
                return hostNb;
        }
        return -1;
    }

    private void createMenuElements() {
        menuElements.addAll(Arrays.asList(new MenuElement[]{
                new MenuDevicesModification(network),
                new MenuDeviceStats(network.getDevices()),
                new MenuGlobalStats(),
                new MenuMACTable(network.getDevices()),
                MenuNetworkTraffic.getInstance(),
                new MenuRoutingTables(network.getDevices()),
                new MenuSendRequest(network.getDevices()),
                new MenuTopology(network.getDevices())
        }));
    }

    public ActionTakenListener getActionTakenListener() {
        for (int i = 0; i < menuElements.size(); i++) {
            if(menuElements.get(i) instanceof ActionTakenListener) {
                return (ActionTakenListener) menuElements.get(i);
            }
        }
        return null;
    }

    @Override
    public void run() {
        this.start();
    }
}
