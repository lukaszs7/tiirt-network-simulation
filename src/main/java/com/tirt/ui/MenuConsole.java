package com.tirt.ui;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

/**
 * Created by Łukasz on 23.05.2017.
 */
public class MenuConsole {
    private static final int BORDER = 20;

    public static final String TITLE = "KONSOLA";
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    private JFrame frame;
    private JTextArea area;

    public MenuConsole(String title, int width, int height) {
        frame = createFrame(title, width, height);
        area = createTextArea(0, 0, WIDTH, HEIGHT);
        JScrollPane scroll = createScrollPane(area);
        frame.getContentPane().add(scroll);
    }

    public void showConsole() {
        frame.setVisible(true);
    }

    public void closeConsole() {
        frame.setVisible(false);
    }

    public void println(String string) {
        area.append(string.concat("\n"));
    }

    public void print(String string) {
        area.append(string);
    }

    public void clear() {
        area.setText("");
    }

    private JFrame createFrame(String title, int sizeX, int sizeY) {
        JFrame frame = new JFrame(title);
        frame.setSize(sizeX, sizeY);
        return frame;
    }

    private JTextArea createTextArea(int xPos, int yPos, int width, int height) {
        JTextArea textArea = new JTextArea();
        textArea.setWrapStyleWord(true);
        textArea.setLineWrap(true);
        textArea.setVisible(true);
        Border border = BorderFactory.createLineBorder(Color.WHITE);
        textArea.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(BORDER, BORDER, BORDER, BORDER)));
        return textArea;
    }

    private JScrollPane createScrollPane(JTextArea textArea) {
        int border = 100;

        JScrollPane areaScrollPane = new JScrollPane(textArea);
        areaScrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        areaScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        areaScrollPane.setBounds(border, border, WIDTH - border, HEIGHT - border);
        return areaScrollPane;
    }
}
