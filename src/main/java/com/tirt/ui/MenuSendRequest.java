package com.tirt.ui;

import com.tirt.network.Device;
import com.tirt.network.IpAddr;
import com.tirt.network.IpHost;
import com.tirt.network.MacAddress;

import java.util.*;

/**
 * Created by Łukasz on 23.05.2017.
 */
public class MenuSendRequest extends Menu implements MenuElement {
    private List<Device> devices;
    private Map<Integer, IpHost> hosts;

    public MenuSendRequest(List<Device> devices) {
        this.devices = devices;
        this.hosts = new HashMap<>();
        hosts = createHostsMap();
    }

    private Map<Integer,IpHost> createHostsMap() {
        Map<Integer, IpHost> mapHosts = new HashMap<>();
        int nextId = 1;
        for(int i = 0; i < devices.size(); i++) {
            if(isHost(devices.get(i))) {
                mapHosts.put(nextId++, (IpHost)devices.get(i));
            }
        }
        return mapHosts;
    }

    private IpHost chooseSender() {
        System.out.println("\nWybierz hosta, z którego wyślesz komunikat (Podaj numer)");
        int hostId = super.nextChooseInput(1, hosts.size());
        return hosts.get(hostId);
    }

    private String chooseRecipient() {
        System.out.println("Podaj ip komputera do którego chcesz wysłać żądanie:");
        return super.nextStringInput();
    }

    private String chooseMessage() {
        System.out.println("Podaj wiadomość:");
        return super.nextLine();
    }

    private void printHosts() {
        System.out.println("Lista urządzeń (Hostów)");
        Set<Map.Entry<Integer, IpHost>> entries = hosts.entrySet();
        Iterator<Map.Entry<Integer, IpHost>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, IpHost> next = iterator.next();
            System.out.println(next.getKey()+". "+deviceName(devices, next.getValue()));
            printOwnIps(next.getValue().getOwnIps());
        }
    }

    private void printOwnIps(Map<MacAddress, IpAddr> ownIps) {
        Set<Map.Entry<MacAddress, IpAddr>> entries = ownIps.entrySet();
        Iterator<Map.Entry<MacAddress, IpAddr>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            Map.Entry<MacAddress, IpAddr> next = iterator.next();
            System.out.println("    MAC: "+next.getKey().getStringAddress()+", IP: "+next.getValue().toString());
        }
    }

    @Override
    public String elementName() {
        return "Wyślij żądanie";
    }

    @Override
    public void start() {
        printHosts();
        IpHost ipHost = chooseSender();
        String ipRec = chooseRecipient();
        String message = chooseMessage();
        ipHost.sendMessage(new IpAddr(ipRec), 0, message);
        super.start();
    }
}
