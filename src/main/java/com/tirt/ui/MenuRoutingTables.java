package com.tirt.ui;

import com.tirt.network.*;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Łukasz on 23.05.2017.
 */
public class MenuRoutingTables extends Menu implements MenuElement {
    private List<Device> devices;
    public MenuRoutingTables(List<Device> devices) {
        this.devices = devices;
    }

    private String createRoutingTable(IpHost ipHost) {
        String header = createRoutingTableHeader();
        StringBuilder builder = new StringBuilder();
        builder.append(header);

        Set<Map.Entry<SubnetAddr, IpAddr>> entrySet = ipHost.getRoutingTable().entrySet();
        Iterator it = entrySet.iterator();
        int ordinal = 1;
        while (it.hasNext()) {
            Map.Entry<SubnetAddr, IpAddr> next = (Map.Entry<SubnetAddr, IpAddr>) it.next();
            SubnetAddr key = next.getKey();
            IpAddr value = next.getValue();
            String row = createRoutingTableRow(ordinal++, key, value);
            builder.append(row);
        }
        return builder.append(closeTable()).toString();
    }

    private String createRoutingTableHeader() {
        return  "+-----------------------+-----------------------+-----------------------+\n"+
                "| Destination           | Netmask               | Gateway               |\n"+
                "+-----------------------+-----------------------+-----------------------+\n";
    }

    private String createRoutingTableRow(int ordinal, SubnetAddr subnetAddr, IpAddr gateway) {
        return String.format("|   %-17s   |   %-17s   |   %-17s   |%n", subnetAddr.addr.toString(), subnetAddr.maskLength, gateway.toString());
    }

    private String closeTable() {
        return "+-----------------------+-----------------------+-----------------------+\n";
    }

    @Override
    public String elementName() {
        return "Podgląd tablic routingu";
    }

    @Override
    public void start() {
        for (int i = 0; i < devices.size(); i++) {
            if(isHost(devices.get(i)) || isRouter(devices.get(i))) {
                IpHost ipHost = (IpHost) devices.get(i);
                System.out.println(deviceName(devices, ipHost) + ":");
                String routingTable = createRoutingTable(ipHost);
                System.out.println(routingTable);
            }
        }
        super.start();
    }
}
