package com.tirt.ui;

import com.tirt.network.*;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Łukasz on 23.05.2017.
 */
public class MenuDevicesModification extends Menu implements MenuElement {
    private static final String DEVICE_QUEUE_CAPACITY = "QueueCapacity";
    private static final String DEVICE_INTERVAL = "Interval";
    public static final String GENERATOR_IP_KEY = "ip";

    private static final String[] DEVICE_PROPS = {DEVICE_QUEUE_CAPACITY, DEVICE_INTERVAL};
    private List<Device> devices;
    private Network network;

    public MenuDevicesModification(Network network) {
        this.network = network;
        this.devices = network.getDevices();
    }

    private String createDevicesList() {
        String list = "";
        StringBuilder builder = new StringBuilder();
        builder.append(list);
        for (int i = 0; i < devices.size(); i++) {
            Device device = devices.get(i);
            String interval = "";
            if (hasInterval(device)) {
                interval = "\n     Interval: " + ((IpHost) device).getInterval();
            }

            String row = deviceName(devices, device) +
                    "\n     QueueCapacity: " + device.getQueueCapacity() + interval;
            builder.append(row + "\n");
        }
        return builder.toString();
    }

    private String createMACsStr(Device device) {
        String macs = "";
        Map<Integer, MacAddress> ownMacs = device.getOwnMacs();
        for (int i = 0; i < ownMacs.size(); i++) {
            macs += ownMacs.get(i).getStringAddress();
            if (i != ownMacs.size() - 1) {
                macs += ",";
            }
        }
        return macs;
    }

    private String createPropsList(Device device) {
        String list = "";
        StringBuilder builder = new StringBuilder();
        builder.append(list);

        for (int i = 0; i < DEVICE_PROPS.length; i++) {
            if (!DEVICE_PROPS[i].equals(DEVICE_INTERVAL) || hasInterval(device))
                builder.append("\n" + (i + 1) + ". " + DEVICE_PROPS[i]);
        }
        return builder.toString();
    }

    private void changeDeviceProperties(String deviceId) {
        Device device = getDevice(devices, deviceId);
        String propsList = createPropsList(device);
        int devicePropsLength = DEVICE_PROPS.length - (hasInterval(device) ? 0 : 1);
        int propertyIdx = nextIntegerWithBackInfo("Zmiana ustawień " + deviceId + propsList, MENU_INDEX_START, devicePropsLength);
        if (propertyIdx == -1) {
            super.start();
            return;
        }

        changeDeviceProperty(device, propertyIdx);
    }

    private void changeDeviceProperty(Device device, int propertyIdx) {
        int value = nextIntegerWithBackInfo("Podaj wartość", 0, Integer.MAX_VALUE);
        if (value == -1) {
            super.start();
            return;
        }

        String deviceName = deviceName(devices, device);
        if (DEVICE_PROPS[propertyIdx - 1].equals(DEVICE_QUEUE_CAPACITY)) {
            int queueCap = Integer.valueOf(value);
            System.out.println("\nZmieniono wartość QueueCapacity dla " + deviceName + "\n");
            device.setQueueCapacity(queueCap);
        } else if (DEVICE_PROPS[propertyIdx - 1].equals(DEVICE_INTERVAL)) {
            int intervalNew = Integer.valueOf(value);
            int intervalOld = device.getInterval();
            System.out.println("\nZmieniono wartość Interval dla " + deviceName + "\n");
            device.setInterval(intervalNew);
            editGenerators(device, intervalOld, intervalNew);
        }
        super.start();
    }

    private void editGenerators(Device host, int intervalOld, int intervalNew) {
        SteadyGenerator generator = network.getDeviceGenerator(host);
        if (generator != null) {
            if (intervalOld > 0 && intervalNew == 0) {
                network.stopGenerator(generator);
            }
            generator.setInterval(intervalNew);
        } else {
            String ip = super.nextLineWithBackInfo("Tworzenie generatora. Wpisz '" + GENERATOR_IP_KEY + "' w przypadku generatora pakietów. Domyślnie generator ramek.");
            network.startGenerator(host, intervalNew, ip);
        }
    }

    private void printDevicesList() {
        String list = createDevicesList();
        System.out.println(list);
    }

    @Override
    public String elementName() {
        return "Modyfikacja właściwości urządzeń";
    }

    @Override
    public void start() {
        printDevicesList();
        String input = nextLineWithBackInfo("Wybierz urządzenie");
        if (isBackwards(input)) {
            super.start();
            return;
        }

        changeDeviceProperties(input);
    }
}
