package com.tirt.network;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by maciej on 11.06.2017.
 */
public class SteadyPacketGenerator extends SteadyGenerator {
    private static Random rand = new Random();
    private IpHost ipHost;

    public SteadyPacketGenerator(IpHost host, Network network, int interval) {
        super(host, network, interval);
        ipHost = host;
    }

    @Override
    public void run() {
        while(running) {
            try {
                Thread.sleep(interval);
            } catch(InterruptedException ex) {
                // ??
            }

            List<IpHost> devices = network.getIpHosts(false);
            IpHost targetDevice;

            do {
                targetDevice = devices.get(rand.nextInt(devices.size()));
            }
            while(targetDevice == this.host);

            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

            Object[] targetIpIfaces = targetDevice.getOwnIps().values().toArray();
            IpAddr targetIp = (IpAddr) targetIpIfaces[rand.nextInt(targetIpIfaces.length)];
            String message = "Message generated on " + dateFormat.format(date);
//            System.out.println("Generating message from " + host.name + " to " + targetIp.toString());

            ipHost.sendMessage(targetIp, rand.nextInt(), message);
        }
    }
}
