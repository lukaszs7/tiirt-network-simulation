package com.tirt.network;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by maciej on 11.06.2017.
 */
public abstract class MarkovGenerator implements Runnable {
    public interface State {

        int nextInterval();
    }

    public static final class SteadyState implements State {

        public int intervalMs;
        public SteadyState(int intervalMs) {
            this.intervalMs = intervalMs;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;

            SteadyState that = (SteadyState) o;

            return intervalMs == that.intervalMs;
        }

        @Override
        public int hashCode() {
            int result = super.hashCode();
            result = 31 * result + intervalMs;
            return result;
        }

        @Override
        public int nextInterval() {
            return intervalMs;
        }

    }

    public static final class ExponentialState implements State {

        public double origin;
        public double lambda;
        public double maximum;
        public ExponentialState(double origin, double lambda, double maximum) {
            assert lambda > 0.0f;
            assert origin > 0.0f;

            this.origin = origin;
            this.lambda = lambda;
            this.maximum = maximum;
        }

        @Override
        public int nextInterval() {
            double unit = 1.0f - rand.nextDouble();  // (0; 1]
            return (int) Math.min(origin - Math.log(unit) / lambda, maximum);
        }

    }

    public static final class Transition {

        public int weight;
        public int next;
        public Transition(int weight, int next) {
            this.weight = weight;
            this.next = next;
        }

    }

    protected Network network;
    private Integer currentStateId = null;
    public HashMap<Integer, State> states = new HashMap<>();
    public HashMap<Integer, ArrayList<Transition>> transitions = new HashMap<>();

    protected static Random rand = new Random();

    public MarkovGenerator(Network network) {
        this.network = network;
    }

    public void addState(int id, State s) {
        states.put(id, s);
    }

    public void setCurrentStateId(int id) {
        currentStateId = id;
    }

    public void addTransition(int from, int to, int weight) {
        assert states.containsKey(from);
        assert states.containsKey(to);

        if (!transitions.containsKey(from)) {
            transitions.put(from, new ArrayList<>());
        }
        transitions.get(from).add(new Transition(weight, to));
    }

    private void transitionRandomly() {
        List<Transition> succ = transitions.get(currentStateId);
        int totalWeight = 0;
        for (Transition t : succ) {
            totalWeight += t.weight;
        }
        int sample = rand.nextInt(totalWeight);
        int rollingSum = 0;
        for (Transition t : succ) {
            if (rollingSum >= sample && rollingSum + t.weight < sample) {
                currentStateId = t.next;
                break;
            }
            rollingSum += t.weight;
        }
    }

    public abstract void generate();

    @Override
    public void run() {
        assert !states.isEmpty();
        if (currentStateId == null)
            currentStateId = (Integer) states.keySet().toArray()[0];
        while(true) {
            try {
                Thread.sleep(states.get(currentStateId).nextInterval());
            } catch(InterruptedException ex) {
                // ??
            }
            generate();
            transitionRandomly();
        }
    }
}
