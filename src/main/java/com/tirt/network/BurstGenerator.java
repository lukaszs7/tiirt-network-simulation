package com.tirt.network;

/**
 * Created by Łukasz on 12.06.2017.
 */
public abstract class BurstGenerator implements Runnable {
    protected Network network;
    protected Device host;
    protected int interval;
    protected int n;
    protected boolean running = true;

    public BurstGenerator(Device host, Network network, int interval, int n) {
        this.network = network;
        this.host = host;
        this.interval = interval;
        if(n >= 0) {
            this.n = n;
        } else {
            this.n = 0;
        }
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public Device getHost() {
        return host;
    }

    public void stopGenerator() {
        running = false;
    }

    @Override
    public String toString() {
        return "SteadyGenerator{" +
                "interval=" + interval +
                ", running=" + running +
                '}';
    }
}
