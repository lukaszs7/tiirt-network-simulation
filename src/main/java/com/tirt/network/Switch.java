package com.tirt.network;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Łukasz on 06.04.2017.
 */
public class Switch extends Device {
    public Switch(Map<Integer, MacAddress> ownMacs, int queueCapacity) {
        super(ownMacs, queueCapacity);
        this.forwardEthernetFrames = true;

    }
    public Switch(int queueCapacity) {
        super(new HashMap<>(), queueCapacity);
        this.forwardEthernetFrames = true;
        HashMap<Integer, MacAddress> macs = new HashMap<>();
        macs.put(1, new MacAddress());

        this.setOwnMacs(macs);
    }
}
