package com.tirt.network;

import java.net.Inet4Address;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * Created by maciej on 21.04.2017.
 */
public class IpPacket {
    public static final short ETH_TYPE = 0x0800;

    public IpAddr source;
    public IpAddr destination;
    public int totalLength;
    public int identification;
    public int fragmentOffset;
    public int timeToLive = 30;
    public ByteBuffer payload;

    // Update this if you add/remove members.
    public static final int HEADER_LENGTH = 24;

    public IpPacket(IpAddr source, IpAddr destination, ByteBuffer payload, int identification) {
        assert payload.remaining() >= ((1 << 16) - HEADER_LENGTH);
        this.source = source;
        this.destination = destination;
        this.payload = payload;
        this.totalLength = payload.remaining();
        this.identification = identification;
        this.fragmentOffset = payload.position();
    }

    private IpPacket() {}

    public static IpPacket fromSlice(IpAddr source, IpAddr destination, ByteBuffer payload, int offset, int length, int identification) {
        ByteBuffer wrap = ByteBuffer.wrap(payload.array(), offset, length);
        return new IpPacket(source, destination, wrap, identification);
    }

    public byte[] serialize() {
        ByteBuffer buf = ByteBuffer.allocate(HEADER_LENGTH + payload.remaining());
        buf.putInt(source.raw);
        buf.putInt(destination.raw);
        buf.putInt(totalLength);
        buf.putInt(identification);
        buf.putInt(fragmentOffset);
        buf.putInt(timeToLive);
        buf.put(payload);

        return buf.array();
    }

    public static IpPacket deserialize(byte[] from) {
        IpPacket p = new IpPacket();

        ByteBuffer buf = ByteBuffer.wrap(from);
        p.source = new IpAddr(buf.getInt());
        p.destination = new IpAddr(buf.getInt());
        p.totalLength = buf.getInt();
        p.identification = buf.getInt();
        p.fragmentOffset = buf.getInt();
        p.timeToLive = buf.getInt();
        p.payload = ByteBuffer.wrap(buf.array(), buf.position(), buf.remaining());

        return p;
    }

    public List<IpPacket> fragment(int mtu) {
        int fragmentSize = mtu - HEADER_LENGTH;
        int fragments = totalLength / fragmentSize + (totalLength % fragmentSize != 0 ? 1 : 0);

        List<IpPacket> packets = new ArrayList<>(fragments);
        int offset = 0;
        for (; offset < totalLength - fragmentSize; offset += fragmentSize)
            packets.add(fromSlice(source, destination, payload, offset, fragmentSize, identification));
        if (offset < totalLength)
            packets.add(fromSlice(source, destination, payload, offset, totalLength - offset, identification));

        return packets;
    }

    public List<byte[]> serializedFragments(int mtu) {
        List<IpPacket> packets = fragment(mtu);
        List<byte[]> result = new ArrayList<>(packets.size());
        for (IpPacket p : packets)
            result.add(p.serialize());
        return result;
    }
}
