package com.tirt.network;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by Łukasz on 12.06.2017.
 */
public class BurstPacketGenerator extends BurstGenerator {
    private static Random rand = new Random();
    private IpHost ipHost;

    public BurstPacketGenerator(IpHost host, Network network, int interval, int n) {
        super(host, network, interval, n);
        this.ipHost = host;
    }

    @Override
    public void run() {
        while(running) {
            try {
                Thread.sleep(interval);
            } catch(InterruptedException ex) {
                // ??
            }

            List<IpHost> devices = network.getIpHosts(false);
            IpHost targetDevice;

            do {
                targetDevice = devices.get(rand.nextInt(devices.size()));
            }
            while(targetDevice == this.host);

            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

            Object[] targetIpIfaces = targetDevice.getOwnIps().values().toArray();
            IpAddr targetIp = (IpAddr) targetIpIfaces[rand.nextInt(targetIpIfaces.length)];
            String message = "Message generated on " + dateFormat.format(date);
//            System.out.println("Generating message from " + host.name + " to " + targetIp.toString());
            for (int i = 0; i < n; i++) {
                ipHost.sendMessage(targetIp, rand.nextInt(), message);
            }
        }
    }
}
