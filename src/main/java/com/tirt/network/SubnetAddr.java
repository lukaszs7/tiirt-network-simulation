package com.tirt.network;

import java.util.*;

public final class SubnetAddr implements Comparable<SubnetAddr> {
    public IpAddr addr;
    public int maskLength;

    public SubnetAddr(String addr) {
        String[] ipAndMask = addr.split("/");
        assert ipAndMask.length == 2;
        this.addr = new IpAddr(ipAndMask[0]);
        this.maskLength = Integer.valueOf(ipAndMask[1]);
        assert this.addr.mask(maskLength).equals(this.addr);
    }

    public SubnetAddr(IpAddr addr, int maskLength) {
        this.addr = addr.mask(maskLength);
        this.maskLength = maskLength;
//        assert this.addr.mask(maskLength).equals(this.addr);
    }

    public SubnetAddr(IpAddr addr, String mask) {
        String[] octets = mask.split("\\.");
        assert octets.length == 4;
        int a = Short.valueOf(octets[0]);
        int b = Short.valueOf(octets[1]);
        int c = Short.valueOf(octets[2]);
        int d = Short.valueOf(octets[3]);

        int maskLength = toCIDR(a) + toCIDR(b) + toCIDR(c) + toCIDR(d);

        this.addr = addr.mask(maskLength);
        this.maskLength = maskLength;
    }

    private int toCIDR(int octet) {
        return Integer.toBinaryString(octet).replace("0","").length();
    }

    public boolean isMoreSpecific(SubnetAddr other) {
        return addr.equals(other.addr) && maskLength > other.maskLength;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof SubnetAddr &&
                maskLength == ((SubnetAddr) obj).maskLength &&
                addr.equals(((SubnetAddr) obj).addr);
    }

    @Override
    public String toString() {
        return addr.toString() + "/" + maskLength;
    }

    @Override
    public int compareTo(SubnetAddr o) {
        // Lexical sort over address first, then the mask length. This is necessary
        // for the entries in Router.routingTable to be arranged correctly.
        return addr.raw != o.addr.raw ? addr.compareTo(o.addr) : maskLength - o.maskLength;
    }

    @Override
    public int hashCode() {
        int result = addr != null ? addr.hashCode() : 0;
        result = 31 * result + maskLength;
        return result;
    }
}
