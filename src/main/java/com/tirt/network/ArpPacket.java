package com.tirt.network;

import java.nio.ByteBuffer;

/**
 * Created by maciej on 06.05.2017.
 */
public class ArpPacket {
    public static final int WHO_HAS = 1;
    public static final int IS_AT = 2;
    public static final short ETH_TYPE = 0x0806;

    int operation;
    MacAddress senderMac;
    IpAddr senderIp;
    MacAddress targetMac;
    IpAddr targetIp;

    private static final int HEADER_LENGTH = 24;

    public ArpPacket(int operation, MacAddress senderMac, IpAddr senderIp, MacAddress targetMac, IpAddr targetIp) {
        this.operation = operation;
        this.senderMac = senderMac;
        this.senderIp = senderIp;
        this.targetMac = targetMac;
        this.targetIp = targetIp;
    }

    public static ArpPacket request(MacAddress senderMac, IpAddr senderIp, IpAddr targetIp) {
        return new ArpPacket(WHO_HAS, senderMac, senderIp, new MacAddress(0), targetIp);
    }

    public static ArpPacket reply(ArpPacket req, MacAddress matchingMac) {
        return new ArpPacket(IS_AT, matchingMac, req.targetIp, req.senderMac, req.senderIp);
    }

    public byte[] serialize() {
        ByteBuffer buf = ByteBuffer.allocate(HEADER_LENGTH);
        buf.putInt(operation);
        buf.put(senderMac.getAddress());
        buf.putInt(senderIp.raw);
        buf.put(targetMac.getAddress());
        buf.putInt(targetIp.raw);

        return buf.array();
    }

    public static ArpPacket deserialize(byte[] from) {
        ByteBuffer buf = ByteBuffer.wrap(from);
        int operation = buf.getInt();
        byte[] senderMacTmp = new byte[6];
        buf.get(senderMacTmp);
        MacAddress senderMac = new MacAddress(senderMacTmp);
        IpAddr senderIp = new IpAddr(buf.getInt());
        byte[] targetMacTmp = new byte[6];
        buf.get(targetMacTmp);
        MacAddress targetMac = new MacAddress(targetMacTmp);
        IpAddr targetIp = new IpAddr(buf.getInt());

        return new ArpPacket(operation, senderMac, senderIp, targetMac, targetIp);
    }

    public EthernetFrame intoFrame(MacAddress dest, MacAddress source) {
        return new EthernetFrame(dest, source, ETH_TYPE, serialize());
    }

    @Override
    public String toString() {
        return "ARP {" +
                (operation == WHO_HAS ? "who-has" : "is-at") +
                ", sender=(" + senderMac + ", " + senderIp + ")" +
                ", target=(" + targetMac + ", " + targetIp + ")" +
                " }";
    }
}
