package com.tirt.network;

import java.util.Random;

public final class IpAddr implements Comparable<IpAddr> {
    public int raw;

    private static Random rng = new Random();

    public IpAddr(String addr) {
        String[] octets = addr.split("\\.");
        assert octets.length == 4;
        int a = Short.valueOf(octets[0]);
        int b = Short.valueOf(octets[1]);
        int c = Short.valueOf(octets[2]);
        int d = Short.valueOf(octets[3]);

        this.raw = (a << 24 | b << 16 | c << 8 | d);
    }

    public IpAddr(int raw) {
        this.raw = raw;
    }

    public IpAddr() {
        this.raw = rng.nextInt();
    }

    public IpAddr mask(int maskLength) {
        return maskLength != 0 ? maskRaw(0xffffffff << (32 - maskLength)) : new IpAddr(0);
    }

    public IpAddr maskRaw(int mask) {
        return new IpAddr(this.raw & mask);
    }

    public boolean belongsToSubnet(IpAddr subnetAddr, int subnetMaskLength) {
        assert subnetAddr.mask(subnetMaskLength).equals(subnetAddr);
        return this.mask(subnetMaskLength).equals(subnetAddr);
    }

    public boolean belongsToSubnet(SubnetAddr sa) {
        return belongsToSubnet(sa.addr, sa.maskLength);
    }

    public boolean belongsToSubnetRaw(IpAddr subnetAddr, int subnetMask) {
        assert subnetAddr.maskRaw(subnetMask).equals(subnetAddr);
        return this.mask(subnetMask).equals(subnetAddr);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof IpAddr && this.raw == ((IpAddr) obj).raw;
    }

    @Override
    public int hashCode() {
        return raw;
    }

    @Override
    public String toString() {
        int a = (raw & 0xff000000) >>> 24;
        int b = (raw & 0x00ff0000) >>> 16;
        int c = (raw & 0x0000ff00) >>> 8;
        int d = raw & 0x000000ff;
        StringBuilder sb = new StringBuilder();
        sb.append(a);
        sb.append('.');
        sb.append(b);
        sb.append('.');
        sb.append(c);
        sb.append('.');
        sb.append(d);
        return sb.toString();
    }

    @Override
    public int compareTo(IpAddr o) {
        long raw = this.raw < 0 ? this.raw + (1L << 32) : this.raw;
        long otherRaw = o.raw < 0 ? o.raw + (1L << 32) : o.raw;
        if (raw - otherRaw < 0)
            return -1;
        else
            return raw == otherRaw ? 0 : 1;
    }
}