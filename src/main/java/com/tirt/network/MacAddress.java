package com.tirt.network;


import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;

import javax.xml.bind.DatatypeConverter;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by Łukasz on 06.04.2017.
 */
public class MacAddress {
    private static Random random = new Random();
    private byte[] address;

    public static final MacAddress BROADCAST = new MacAddress(0xffffffffffffL);

    public MacAddress() {
        this.address = new byte[6];
        random.nextBytes(this.address);
    }

    public MacAddress(long mac) {
        this.address = new byte[6];
        this.address[0] = (byte) (mac & 0xffL);
        this.address[1] = (byte) ((mac & (0xffL << 8)) >>> 8);
        this.address[2] = (byte) ((mac & (0xffL << 16)) >>> 16);
        this.address[3] = (byte) ((mac & (0xffL << 24)) >>> 24);
        this.address[4] = (byte) ((mac & (0xffL << 32)) >>> 32);
        this.address[5] = (byte) ((mac & (0xffL << 40)) >>> 40);
    }

    public MacAddress(byte[] address) {
        this.setAddress(address);
    }

    public MacAddress(String address) {
        byte[] array = convertToByteArray(address);
        this.address = array;
    }

    public String getStringAddress() {
        return convertToString(this.address);
    }

    public static byte[] convertToByteArray(String address) {
        String bytes = address.replaceAll("-", "");
        return DatatypeConverter.parseHexBinary(bytes);
    }

    public static String convertToString(byte[] address) {
        StringBuilder sb = new StringBuilder(HexBin.encode(address).toLowerCase());
        sb.insert(10, '-');
        sb.insert(8, '-');
        sb.insert(6, '-');
        sb.insert(4, '-');
        sb.insert(2, '-');
        return sb.toString();
    }

    public byte[] getAddress() {
        return this.address;
    }

    public void setAddress(byte[] address) {
        if (address == null || address.length < 6) {
            throw new IllegalArgumentException();
        } else {
            this.address = address.clone();
        }
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MacAddress))
            return false;
        MacAddress otherMac = (MacAddress) other;

        return other != null &&
            address[0] == otherMac.address[0] &&
            address[1] == otherMac.address[1] &&
            address[2] == otherMac.address[2] &&
            address[3] == otherMac.address[3] &&
            address[4] == otherMac.address[4] &&
            address[5] == otherMac.address[5];
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(address);
    }

    @Override
    public String toString() {
        return getStringAddress();
    }
}

