package com.tirt.network;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by Łukasz on 12.06.2017.
 */
public class BurstFrameGenerator extends BurstGenerator {
    private Random rand = new Random();

    public BurstFrameGenerator(Device host, Network network, int interval, int n) {
        super(host, network, interval, n);
    }

    @Override
    public void run() {
        while(running) {
            try {
                Thread.sleep(interval);
            } catch(InterruptedException ex) {
                // ??
            }

            List<IpHost> devices = network.getIpHosts(false);
            Device targetDevice;

            do {
                targetDevice = devices.get(rand.nextInt(devices.size()));
            }
            while(targetDevice == this.host);

            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

            MacAddress targetMac = (MacAddress)targetDevice.getOwnMacs().values().toArray()[0];
            String message = "Message generated on " + dateFormat.format(date);
            //System.out.println("Generating message from " + sourceMac.toString() + " to " + targetMac.toString());
            for (int i = 0; i < n; i++) {
                host.sendMessage(targetMac, message);
            }
        }
    }
}
