package com.tirt.network;

import com.tirt.ui.MenuDevicesModification;

import java.util.*;

/**
 * Created by Łukasz on 07.04.2017.
 */
public class Network {
    private List<Device> devices;
    private List<Thread> runners;
    private List<SteadyGenerator> generators;
    private Map<SteadyGenerator, Thread> generatorsThreads;

    public Network(List<Device> devices) {
        this.devices = devices;
        this.runners = new ArrayList<>();
        this.generatorsThreads = new HashMap<>();
    }

    public Network(List<Device> devices, List<SteadyGenerator> generators) {
        this(devices);
        this.generators = generators;
    }

    public Device getDeviceFromGenerator(SteadyGenerator steadyGenerator) {
        for (int i = 0; i < generators.size(); i++) {
            if(generators.get(i) == steadyGenerator) {
                return generators.get(i).getHost();
            }
        }
        return null;
    }

    public SteadyGenerator getDeviceGenerator(Device device) {
        for (int i = 0; i < generators.size(); i++) {
            if(generators.get(i).getHost() == device) {
                return generators.get(i);
            }
        }
        return null;
    }

    public void createGenerator(SteadyGenerator steadyGenerator) {
        generators.add(steadyGenerator);
        Thread th = new Thread(steadyGenerator);
        generatorsThreads.put(steadyGenerator, th);
        th.start();
    }

    public static Device findDeviceByMac(List<Device> devices, MacAddress addr) {
        for (Device d : devices) {
            if (d.hasMacAddress(addr)) {
                return d;
            }
        }
        return null;
    }

    public static Device findDeviceByMac(List<Device> devices, byte[] addrBytes) {
        MacAddress addr = new MacAddress(addrBytes);
        return findDeviceByMac(devices, addr);
    }

    public Device[] connectDevicesByMacs(MacAddress addr1, MacAddress addr2) {
        Device d1 = findDeviceByMac(devices, addr1);
        Device d2 = findDeviceByMac(devices, addr2);
        if (d1 != null && d2 != null) {
            d1.connectDevice(addr1, d2, addr2);
            Device[] pair = new Device[2];
            pair[0] = d1;
            pair[1] = d2;
            return pair;
        }
        return null;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public List<IpHost> getIpHosts(boolean includeRouters) {
        ArrayList<IpHost> result = new ArrayList<>();
        for (Device d : devices) {
            if (d instanceof IpHost && (includeRouters == d instanceof Router)) {
                result.add((IpHost) d);
            }
        }
        return result;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public void go() {
        for (Device d : devices) {
            Thread th = new Thread(d);
            runners.add(th);
            th.start();
        }

        for (SteadyGenerator g : generators) {
            Thread th = new Thread(g);
            generatorsThreads.put(g, th);
            th.start();
        }
    }

    public void block() {
        try {
            for (Thread th : runners)
                th.join();
            Collection<Thread> values = generatorsThreads.values();
            values.stream().forEach(th -> {
                try {
                    th.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        } catch (InterruptedException ex) {
            // pass
        }
    }

    @Override
    public String toString() {
        return "Network{" +
                "devices=" + devices +
                '}';
    }

    public void stopGenerator(SteadyGenerator generator) {
        if(generator != null) {
            Map<SteadyGenerator, Thread> gens = getGeneratorsThreads();
            Thread thread = gens.get(generator);
            generator.stopGenerator();
            generator.setInterval(0);
            if(thread != null) {
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            generators.remove(generator);
            generatorsThreads.remove(generator);
        }
    }

    public void startGenerator(Device host, int interval, String ip) {
        SteadyGenerator generator;
        if(ip != null && ip.equals(MenuDevicesModification.GENERATOR_IP_KEY)) {
            IpHost ipHost = (IpHost) host;
            generator = new SteadyPacketGenerator(ipHost, this, interval);
        } else {
            generator = new SteadyFrameGenerator(host, this, interval);
        }
        createGenerator(generator);
    }

    public void printGens() {
        generators.stream().forEach(System.out::println);
    }

    public void setGenerators(List<SteadyGenerator> generators) {
        this.generators = generators;
    }

    public Map<SteadyGenerator, Thread> getGeneratorsThreads() {
        return generatorsThreads;
    }
}

