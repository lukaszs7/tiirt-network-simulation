package com.tirt.network;

import java.util.Random;

/**
 * Created by Łukasz on 11.06.2017.
 */
public abstract class SteadyGenerator implements Runnable {
    protected Network network;
    protected Device host;
    protected int interval;
    protected boolean running = true;

    public SteadyGenerator(Device host, Network network, int interval) {
        this.network = network;
        this.host = host;
        this.interval = interval;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public Device getHost() {
        return host;
    }

    public void stopGenerator() {
        running = false;
    }

    @Override
    public String toString() {
        return "SteadyGenerator{" +
                "interval=" + interval +
                ", running=" + running +
                '}';
    }
}
