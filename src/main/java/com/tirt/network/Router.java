package com.tirt.network;

import java.util.*;

/**
 * Created by maciej on 22.04.2017.
 */
public class Router extends IpHost {
    public Router(Map<Integer, MacAddress> ownMacs, int queueCapacity, IpAddr defaultGateway) {
        super(ownMacs, queueCapacity, defaultGateway);
        this.forwardEthernetFrames = true;
        this.forwardIpPackets = true;
    }
}
