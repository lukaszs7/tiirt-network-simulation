package com.tirt.network;
import com.tirt.ui.MenuNetworkTraffic;
import com.tirt.utils.ActionTakenListener;
import com.tirt.utils.EventType;
import com.tirt.utils.StatsManager;

import java.util.*;
import java.util.concurrent.*;

/**
 * Created by Łukasz on 06.04.2017.
 */

public class Device implements Runnable {
    public static final class FrameIfacePair {
        public final EthernetFrame frame;
        public final Integer iface;

        public FrameIfacePair(EthernetFrame frame, Integer iface) {
            this.frame = frame;
            this.iface = iface;
        }
    }

    public String name = "(null)";

    protected Queue<FrameIfacePair> incomingQueue;
    protected Queue<FrameIfacePair> outgoingQueue;
    protected Map<MacAddress, Integer> macTable;
    private List<ActionTakenListener> listeners = new ArrayList<>();
    public Map<Integer, MacAddress> ownMacs;
    private int maxConnectedDevices;
    private int queueCapacity;
    protected List<Device> connectedDevices;
    private List<MacAddress> macQueryLruCache;
    private int maxMacQueryLruCacheSize = 100;
    protected boolean forwardEthernetFrames = false;
    protected int interval = 0;

    private static final int DEFAULT_DEVICE_CAP = 1;

    public Device(Map<Integer, MacAddress> ownMacs, int queueCapacity) {
        this.queueCapacity = queueCapacity;
        int deviceCap = ownMacs.isEmpty() ? DEFAULT_DEVICE_CAP : Collections.max(ownMacs.keySet()) + 1;
        this.connectedDevices = new ArrayList<>(deviceCap);
        for (int it = 0; it < deviceCap; ++it)
            this.connectedDevices.add(null);
        this.maxConnectedDevices = deviceCap;
        this.incomingQueue = new ConcurrentLinkedQueue<>();
        this.outgoingQueue = new ConcurrentLinkedQueue<>();
        this.ownMacs = ownMacs;
        this.macTable = new HashMap<>();
        this.macQueryLruCache = new ArrayList<>();
        addListener(StatsManager.getInstance());
        addListener(MenuNetworkTraffic.getInstance());
    }

    public void receive(Device sender, EthernetFrame frame) {
        assert isDeviceConnected(sender);

        if (frame != null) {
            HashMap<String, Object> parameters = new HashMap<>();
            parameters.put("frame", frame);
            parameters.put("sender", this);

            // TODO: figure out how to get rid of the .size() call here, since it's O(n) and not very reliable with wait-freedom
            if (incomingQueue.size() <= queueCapacity) {
                incomingQueue.add(new FrameIfacePair(frame, connectedDevices.indexOf(sender)));
            } else {
                this.reportActionTaken(EventType.FRAME_DROPPED, parameters);
            }
        }
    }

    public void send(EthernetFrame frame) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("frame", frame);
        parameters.put("sender", this);

        if (frame != null) {
            // TODO: figure out how to get rid of the .size() call here, since it's O(n) and not very reliable with wait-freedom
            if (outgoingQueue.size() <= queueCapacity) {
                outgoingQueue.add(new FrameIfacePair(frame, macTable.getOrDefault(frame.getRecipient(), -1)));
            } else {
                this.reportActionTaken(EventType.FRAME_DROPPED, parameters);
            }
        }
    }

    public void connectDevice(MacAddress ownMac, Device device, MacAddress otherMac) {
        // ownMacs must already be populated by the deserializer
        assert connectedDevices.size() < maxConnectedDevices;
        assert ownMacs.containsValue(ownMac);
        assert device.ownMacs.containsValue(otherMac);

        connectDevice(ownMac, device, otherMac, true);
    }

    // If this and the other device have at least one available interface,
    // connect them and return MACs of those interfaces (otherwise null)
    public MacAddress[] connectDevice(Device device) {
        assert connectedDevices.size() < maxConnectedDevices;
        assert Collections.frequency(device.connectedDevices, null) > 0;

        MacAddress ownMac = findUnusedIface();
        MacAddress otherMac = device.findUnusedIface();

        if (ownMac == null || otherMac == null)
            return null;

        connectDevice(ownMac, device, otherMac, true);

        MacAddress[] pair = new MacAddress[2];
        pair[0] = ownMac;
        pair[1] = otherMac;
        return pair;
    }

    public MacAddress findUnusedIface() {
        int phy = connectedDevices.indexOf(null);
        if (phy == -1)
            return null;

        // this method is meant for setting up ad-hoc networks without
        // a deserializer that would set up interface addresses, so we can't
        // rely on ownMacs being populated -- potentially insert random MAC
        // and return that
        //if (!ownMacs.containsKey(phy))
        //    ownMacs.put(phy, new MacAddress());
        return ownMacs.get(phy);
    }

    protected void connectDevice(MacAddress ownMac, Device device, MacAddress otherMac, boolean backlink) {
        if (!isDeviceConnected(device)) {
            int phy = -1;
            for (int it = 0; it < maxConnectedDevices; ++it) {
                if (ownMacs.get(it).equals(ownMac)) {
                    phy = it;
                    break;
                }
            }
            connectedDevices.set(phy, device);
        }
        if (backlink)
            device.connectDevice(otherMac, this, ownMac, false);
    }

    public boolean isDeviceConnected(Device device) {
        return connectedDevices.indexOf(device) != -1;
    }

    public List<Device> getConnectedDevices() {
        return this.connectedDevices;
    }

    protected void reportActionTaken(EventType type, Map<String, Object> arguments) {
        onActionTaken(type, arguments);
    }

    @Override
    public void run() {
        while (true) {
            int sleepTime = 1000 / this.queueCapacity;

            HashMap<String, Object> parameters = new HashMap<>();
            FrameIfacePair fi = incomingQueue.poll();
            if (fi != null) {
                parameters.put("frame", fi.frame);
                parameters.put("sender", this);
                this.reportActionTaken(EventType.FRAME_RECEIVED, parameters);

                process(fi);

            }

            FrameIfacePair task = outgoingQueue.poll();
            if (task != null) {
                parameters.put("frame", task.frame);
                parameters.put("sender", this);

                if (task.iface != null && task.iface != -1 && !task.frame.isBroadcast()) {
                    this.reportActionTaken(EventType.FRAME_SENT, parameters);
                    getConnectedDevices().get(task.iface).receive(this, task.frame);
                } else {
                    int sourceInterface = macTable.getOrDefault(task.frame.getSender(), -1);

                    for (int it = 0; it < connectedDevices.size(); ++it) {
                        Device connectedDevice = connectedDevices.get(it);

                        if (connectedDevice != null) {
                            if (sourceInterface == it)
                                continue;

                            this.reportActionTaken(EventType.FRAME_SENT, parameters);

                            connectedDevice.receive(this, task.frame);
                        }
                    }
                }
            }

            try {
                Thread.sleep(sleepTime);
            } catch(Exception ex) {

            }
        }
    }

    protected void process(FrameIfacePair fi) {
        if (!forwardEthernetFrames || fi.frame.getRecipient().equals(ownMacs.get(macTable.get(fi.frame.getRecipient())))) {
            Map<String, Object> args = new HashMap<>();
            args.put("sender", this);
            args.put("frame", fi.frame);
            onActionTaken(EventType.FRAME_DROPPED_EXPLICITLY, args);
            return;
        }

        // It takes time to update MAC table and decide where to send the frame
        try {
            Thread.sleep(10);
        }
        catch(InterruptedException ex) {
            // ??
        }

        updateMacTable(fi.frame.getSender(), fi.iface);
        send(fi.frame);
    }

    protected void evictOldestMacTableEntry() {
        MacAddress oldestMac = macQueryLruCache.remove(0);
        macTable.remove(oldestMac);
    }

    protected void updateMacQueryCache(MacAddress address) {
        int addrIndex = macQueryLruCache.indexOf(address);
        if (addrIndex == -1 && macQueryLruCache.size() >= maxMacQueryLruCacheSize)
            evictOldestMacTableEntry();
        if (addrIndex != macQueryLruCache.size() - 1) {
            MacAddress next = macQueryLruCache.get(addrIndex + 1);
            macQueryLruCache.set(addrIndex + 1, address);
            macQueryLruCache.set(addrIndex, next);
        }
    }

    protected void updateMacTable(MacAddress address, Integer iface) {
        if (macTable.containsKey(address) && macTable.get(address).equals(iface) || ownMacs.containsValue((address)))
            return; // No work to do

        if (macTable.containsKey(address)) {
            Map<String, Object> args = new HashMap<>();
            args.put("sender", this);
            args.put("addr", address);
            args.put("new-iface", iface);
            args.put("old-iface", macTable.get(address));
            onActionTaken(EventType.MAC_TABLE_ENTRY_OVERWRITTEN, args);
        }
        Map<String, Object> args = new HashMap<>();
        args.put("sender", this);
        args.put("addr", address);
        args.put("iface", iface);
        onActionTaken(EventType.MAC_TABLE_UPDATED, args);
        macTable.put(address, iface);
//        updateMacQueryCache(address);
    }

    public void sendMessage(MacAddress dest, String msg) {
        Integer targetPhy = macTable.get(dest);
        if (targetPhy != null) {
            MacAddress sourceMac = ownMacs.get(targetPhy);
            EthernetFrame frame = new EthernetFrame(dest, sourceMac, (short) 0xffff, msg);
            send(frame);
            HashMap<String, Object> args = new HashMap<>();
            args.put("sender", this);
            args.put("frame", frame);
            reportActionTaken(EventType.FRAME_SENT_NEW, args);
        } else {
            for (MacAddress sourceMac : ownMacs.values()) {
                EthernetFrame frame = new EthernetFrame(dest, sourceMac, (short) 0xffff, msg);
                send(frame);
                HashMap<String, Object> args = new HashMap<>();
                args.put("sender", this);
                args.put("frame", frame);
                reportActionTaken(EventType.FRAME_SENT_NEW, args);
            }
        }
    }

    protected void onActionTaken(EventType eventType, Map<String, Object> args) {
        for (int i = 0; i < listeners.size(); i++) {
            listeners.get(i).onActionTaken(eventType, args);
        }
    }

    protected void addListener(ActionTakenListener listener) {
        if(listener != null) {
            listeners.add(listener);
        }
    }

    public int getQueueCapacity() {
        return queueCapacity;
    }

    public void setQueueCapacity(int queueCapacity) {
        this.queueCapacity = queueCapacity;
    }

    public Map<MacAddress, Integer> getMacTable() {
        return macTable;
    }

    public void setMacTable(Map<MacAddress, Integer> macTable) {
        this.macTable = macTable;
    }

    public Map<Integer, MacAddress> getOwnMacs() {
        return ownMacs;
    }

    public void setOwnMacs(Map<Integer, MacAddress> ownMacs) {
        this.ownMacs = ownMacs;
    }

    public boolean hasMacAddress(MacAddress addr) {
        return ownMacs.containsValue(addr);
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
