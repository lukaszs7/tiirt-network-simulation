package com.tirt.network;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Łukasz on 06.04.2017.
 */
public class Host extends Device {
    public Host(Map<Integer, MacAddress> ownMacs, int queueCapacity) {
        super(ownMacs, queueCapacity);
    }

    public Host(int queueCapacity) {
        super(new HashMap<>(), queueCapacity);
        HashMap<Integer, MacAddress> macs = new HashMap<>();
        macs.put(0, new MacAddress());

        this.setOwnMacs(macs);
    }

    public void sendMessage(MacAddress recipient, String message) {
        EthernetFrame frame = new EthernetFrame(recipient, ownMacs.get(macTable.get(recipient)), (short) 0xffff, message);
        this.send(frame);
    }

    @Override
    public void process(FrameIfacePair fi) {
        if (!fi.frame.getRecipient().equals(ownMacs.get(macTable.get(fi.frame.getRecipient()))))
            return;  // non-promiscuous mode - don't process frames not meant for you

        updateMacTable(fi.frame.getSender(), fi.iface);

        StringBuilder sb = new StringBuilder(fi.frame.getData().length);
        for (byte b : fi.frame.getData()) {
            sb.append((char) b);
        }

        System.out.println(fi.frame.getRecipient() + " received msg: '" + sb.toString() + "'");
    }
}
