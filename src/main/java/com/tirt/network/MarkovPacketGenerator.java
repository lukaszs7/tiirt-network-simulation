package com.tirt.network;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by maciej on 11.06.2017.
 */
public class MarkovPacketGenerator extends MarkovGenerator {
    protected IpHost host;

    public MarkovPacketGenerator(Network network, IpHost host) {
        super(network);
        this.host = host;
    }

    @Override
    public void generate() {
        List<IpHost> devices = network.getIpHosts(false);
        IpHost targetDevice = devices.get(rand.nextInt(devices.size()));

        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

        Object[] targetIpIfaces = targetDevice.getOwnIps().values().toArray();
        IpAddr targetIp = (IpAddr) targetIpIfaces[rand.nextInt(targetIpIfaces.length)];
        String message = "Message generated on " + dateFormat.format(date);
//            System.out.println("Generating message from " + host.name + " to " + targetIp.toString());

        host.sendMessage(targetIp, rand.nextInt(), message);
    }
}
