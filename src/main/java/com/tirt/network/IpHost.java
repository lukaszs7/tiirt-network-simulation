package com.tirt.network;

import com.tirt.utils.EventType;

import java.util.*;
import java.nio.ByteBuffer;

/**
 * Created by maciej on 06.05.2017.
 */
public class IpHost extends Device {
    public static final class LinkHop {
        public MacAddress link;
        public IpAddr hop;

        public LinkHop(MacAddress link, IpAddr hop) {
            this.link = link;
            this.hop = hop;
        }
    }

    public static final class IpStreamId {
        public IpAddr source;
        public IpAddr destination;
        public int identification;
        public int totalLength;

        public IpStreamId(IpAddr source, IpAddr destination, int identification, int totalLength) {
            this.source = source;
            this.destination = destination;
            this.identification = identification;
            this.totalLength = totalLength;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            IpStreamId that = (IpStreamId) o;

            if (identification != that.identification) return false;
            if (totalLength != that.totalLength) return false;
            if (source != null ? !source.equals(that.source) : that.source != null)
                return false;
            return destination != null ? destination.equals(that.destination) : that.destination == null;
        }

        @Override
        public int hashCode() {
            int result = source != null ? source.hashCode() : 0;
            result = 31 * result + (destination != null ? destination.hashCode() : 0);
            result = 31 * result + identification;
            result = 31 * result + totalLength;
            return result;
        }

        public static IpStreamId fromPacket(IpPacket p) {
            return new IpStreamId(p.source, p.destination, p.identification, p.totalLength);
        }
    }

    public static final class IpPayloadGatherState {
        public byte[] bytes;
        public int received;

        public IpPayloadGatherState(int totalLength) {
            this.bytes = new byte[totalLength];
            this.received = 0;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            IpPayloadGatherState that = (IpPayloadGatherState) o;

            if (received != that.received) return false;
            return Arrays.equals(bytes, that.bytes);
        }

        @Override
        public int hashCode() {
            int result = Arrays.hashCode(bytes);
            result = 31 * result + received;
            return result;
        }

        public boolean isComplete() {
            return received == bytes.length;
        }

        public void fill(IpPacket frag) {
            int newBytes = frag.payload.remaining();
            frag.payload.duplicate().get(bytes, frag.fragmentOffset, newBytes);
            received += newBytes;
        }
    }


    public static final class DeferredMsg {
        public final IpAddr gateway;
        public final IpAddr dest;
        public final int id;
        public final String msg;

        public DeferredMsg(IpAddr gateway, IpAddr dest, int id, String msg) {
            this.gateway = gateway;
            this.dest = dest;
            this.id = id;
            this.msg = msg;
        }
    }

    protected HashMap<IpAddr, ArrayList<IpPacket>> ipQueue;
    protected HashMap<IpAddr, MacAddress> arpTable;
    protected TreeMap<SubnetAddr, IpAddr> routingTable;
    public HashMap<MacAddress, IpAddr> ownIps;
    protected HashMap<IpStreamId, IpPayloadGatherState> ipFragmentDb;
    protected ArrayList<DeferredMsg> deferredMsgs = new ArrayList<>();
    protected boolean forwardIpPackets = false;

    protected static final int MAX_ROUTE_LOOKUP_ITERATIONS = 5;

    public IpHost(Map<Integer, MacAddress> ownMacs, int queueCapacity, IpAddr defaultGateway) {
        super(ownMacs, queueCapacity);
        this.ipQueue = new HashMap<>();
        this.arpTable = new HashMap<>();
        this.routingTable = new TreeMap<>();
        this.ownIps = new HashMap<>();
        routingTable.put(new SubnetAddr(new IpAddr(0), 0), defaultGateway);
    }

    @Override
    public void process(Device.FrameIfacePair fi) {
        updateMacTable(fi.frame.getSender(), fi.iface);
        if (!fi.frame.isBroadcast() && !fi.frame.getRecipient().equals(ownMacs.get(fi.iface))) {
            if (forwardEthernetFrames)
                send(fi.frame);
        } else {
            switch (fi.frame.getType()) {
                case IpPacket.ETH_TYPE:
                    processIpPacket(fi);
                    break;
                case ArpPacket.ETH_TYPE:
                    processArpPacket(fi);
                    break;
            }
        }
    }

    protected void processArpPacket(Device.FrameIfacePair fi) {
        ArpPacket p = ArpPacket.deserialize(fi.frame.getData());
        arpTable.put(p.senderIp, p.senderMac);
        if (p.operation == ArpPacket.WHO_HAS) {
            MacAddress ownMac = ownMacs.get(fi.iface);
            if (p.targetIp.equals(ownIps.get(ownMac))) {
                send(ArpPacket.reply(p, ownMac).intoFrame(p.senderMac, ownMac));
            }
        } else if (p.operation == ArpPacket.IS_AT) {
            flushDeferredIpPackets(p.senderIp, p.senderMac);
            flushDeferredMessages(p.senderIp);
        }
    }

    private void flushDeferredIpPackets(IpAddr hop, MacAddress link) {
        assert arpTable.containsKey(hop);

        List<IpPacket> deferredPackets = ipQueue.remove(hop);
        if (deferredPackets == null)
            return;  // no packets have been deferred for this route

        for (IpPacket dp : deferredPackets)
            send(new EthernetFrame(link, ownMacs.get(macTable.get(link)), IpPacket.ETH_TYPE, dp.serialize()));
    }

    protected void processIpPacket(Device.FrameIfacePair fi) {
        IpPacket p = IpPacket.deserialize(fi.frame.getData());
        if (p.destination.equals(ownIps.get(ownMacs.get(fi.iface)))) {
            submitPacket(p);
            return;
        }
        if (!forwardIpPackets) {
            return;
        }
        forwardIpPacket(fi.frame, p);
    }

    private void submitPacket(IpPacket p) {
        if (p.fragmentOffset == 0 && p.payload.remaining() == p.totalLength) {
            handleCompletePacket(p);
            return;
        }

        IpStreamId id = IpStreamId.fromPacket(p);
        IpPayloadGatherState payloadState = ipFragmentDb.get(id);
        if (payloadState == null) {
            ipFragmentDb.put(id, new IpPayloadGatherState(id.totalLength));
            payloadState = ipFragmentDb.get(id);
        }
        payloadState.fill(p);
        if (payloadState.isComplete()) {
            ipFragmentDb.remove(id);
            handleCompletePacket(new IpPacket(p.source, p.destination, ByteBuffer.wrap(payloadState.bytes), p.identification));
        }
    }

    public void handleCompletePacket(IpPacket p) {
        ByteBuffer bb = p.payload.duplicate();
        StringBuffer sb = new StringBuffer(bb.remaining());
        while (bb.hasRemaining())
            sb.append((char) bb.get());
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("packet", p);
        parameters.put("message", sb.toString());
        reportActionTaken(EventType.PACKET_ASSEMBLED, parameters);
    }

    protected MacAddress lookupOwnMac(IpAddr ip) {
        for (Map.Entry<MacAddress, IpAddr> e : ownIps.entrySet()) {
            if (e.getValue().equals(ip)) {
                return e.getKey();
            }
        }
        return null;
    }

    protected void forwardIpPacket(EthernetFrame wrappingFrame, IpPacket p) {
        IpAddr nextHop = p.destination;
        LinkHop linkRoutePair = findNextLink(nextHop);

        // If no suitable next-hop address is found, fall back to the destination
        // address, so we can potentially discover it later through ARP.
        if (linkRoutePair.hop == null)
            linkRoutePair.hop = p.destination;

        if (linkRoutePair.link != null) {
            wrappingFrame.setRecipient(linkRoutePair.link);
            wrappingFrame.setSender(ownMacs.get(macTable.get(linkRoutePair.link)));
            send(wrappingFrame);
        } else {
            if (ownIps.containsValue(linkRoutePair.hop)) {
                // I'm not 100% sure about this, but I think we can send a constrained who-has here, since we've already
                // determined which iface must lead to the destination
                issueConstrainedArpRequest(p, p.destination, linkRoutePair.hop);
            } else {
                issueArpRequest(p, linkRoutePair.hop);
            }
        }
    }

    protected LinkHop findNextLink(IpAddr nextHop) {
        int it = 0;
        boolean defaulted = false;
        MacAddress nextLink = null;
        do {
            nextLink = arpTable.get(nextHop);
            if (nextLink != null || defaulted) {
                return new LinkHop(nextLink, nextHop);
            } else {
                Map.Entry<SubnetAddr, IpAddr> nextHopCandidate = routingTable.floorEntry(new SubnetAddr(nextHop, 32));
                if (nextHopCandidate != null && nextHop.belongsToSubnet(nextHopCandidate.getKey())) {
                    nextHop = nextHopCandidate.getValue();
                } else {
                    nextHop = routingTable.get(new SubnetAddr(new IpAddr(0), 0));
                    defaulted = true;
                }
            }
            it++;
        } while (nextHop != null && it < MAX_ROUTE_LOOKUP_ITERATIONS);
        return new LinkHop(null, nextHop);
    }

    protected void issueArpRequest(IpPacket p, IpAddr route) {
        deferIpPacket(p, route);
        sendArpRequestFrame(route);
    }

    protected void issueConstrainedArpRequest(IpPacket p, IpAddr route, IpAddr ownIpConstraint) {
        deferIpPacket(p, route);
        sendConstrainedArpRequestFrame(route, ownIpConstraint);
    }

    protected void issueArpRequest(List<IpPacket> fragments, IpAddr route) {
        for (IpPacket frag : fragments)
            deferIpPacket(frag, route);
        sendArpRequestFrame(route);
    }

    private void sendArpRequestFrame(IpAddr route) {
        for (MacAddress outMac : ownMacs.values()) {
            EthernetFrame frame = ArpPacket.request(outMac, ownIps.get(outMac), route).intoFrame(MacAddress.BROADCAST, outMac);
            send(frame);
            HashMap<String, Object> args = new HashMap<>();
            args.put("sender", this);
            args.put("frame", frame);
            reportActionTaken(EventType.FRAME_SENT_NEW, args);
        }
    }

    private void sendConstrainedArpRequestFrame(IpAddr route, IpAddr ownIpConstraint) {
        for (MacAddress outMac : ownMacs.values()) {
            if (!ownIps.get(outMac).equals(ownIpConstraint))
                continue;
            send(ArpPacket.request(outMac, ownIps.get(outMac), route).intoFrame(MacAddress.BROADCAST, outMac));
        }
    }

    protected void deferIpPacket(IpPacket p, IpAddr route) {
        ArrayList<IpPacket> packetsWaiting = ipQueue.get(route);
        if (packetsWaiting == null) {
            ipQueue.put(route, new ArrayList<>());
            packetsWaiting = ipQueue.get(route);
        }
        if (p != null)
            packetsWaiting.add(p);
    }

    public void sendMessage(IpAddr dest, int id, String msg) {
        LinkHop linkHop = findNextLink(dest);
        if (linkHop.link == null) {
            assert linkHop.hop != null;
            deferMessage(linkHop.hop, dest, id, msg);
            sendArpRequestFrame(linkHop.hop);
            return;
        }
        IpPacket p = new IpPacket(ownIps.get(ownMacs.get(macTable.get(linkHop.link))), dest, ByteBuffer.wrap(msg.getBytes()), id);
        List<IpPacket> fragments = p.fragment(EthernetFrame.GLOBAL_MTU);
        assert fragments.size() > 0;

        for (IpPacket frag : fragments) {
            EthernetFrame newFrame = new EthernetFrame(linkHop.link, ownMacs.get(macTable.get(linkHop.link)), IpPacket.ETH_TYPE, frag.serialize());
            send(newFrame);
            HashMap<String, Object> args = new HashMap<>();
            args.put("sender", this);
            args.put("frame", newFrame);
            reportActionTaken(EventType.FRAME_SENT_NEW, args);
        }
    }

    private void deferMessage(IpAddr gateway, IpAddr dest, int id, String msg) {
        deferredMsgs.add(new DeferredMsg(gateway, dest, id, msg));
    }

    private void flushDeferredMessages(IpAddr gateway) {
        ArrayList<DeferredMsg> destined = new ArrayList<>();
        for (DeferredMsg msg : deferredMsgs) {
            if (msg.gateway.equals(gateway))
                destined.add(msg);
        }
        if (!destined.isEmpty()) {
            deferredMsgs.removeAll(destined);
            for (DeferredMsg msg : destined)
                sendMessage(msg.dest, msg.id, msg.msg);
        }
    }

    public TreeMap<SubnetAddr, IpAddr> getRoutingTable() {
        return routingTable;
    }

    public void mergeRoutingTable(TreeMap<SubnetAddr, IpAddr> routingTable) {
        this.routingTable.putAll(routingTable);
    }

    public HashMap<MacAddress, IpAddr> getOwnIps() {
        return ownIps;
    }

    public void setOwnIps(HashMap<MacAddress, IpAddr> ownIps) {
        this.ownIps = ownIps;
    }
}
