package com.tirt.network;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by maciej on 11.06.2017.
 */
public class MarkovFrameGenerator extends MarkovGenerator {
    protected Device host;

    public MarkovFrameGenerator(Network network, Device host) {
        super(network);
    }

    @Override
    public void generate() {
        List<Device> devices = network.getDevices();
        Device targetDevice;

        do {
            targetDevice = devices.get(rand.nextInt(devices.size()));
        }
        while(targetDevice == this.host);

        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

        MacAddress sourceMac = (MacAddress)host.getOwnMacs().values().toArray()[0];
        MacAddress targetMac = (MacAddress)targetDevice.getOwnMacs().values().toArray()[0];
        String message = "Message generated on " + dateFormat.format(date);
        //System.out.println("Generating message from " + sourceMac.toString() + " to " + targetMac.toString());

        EthernetFrame frame = new EthernetFrame(targetMac, sourceMac, (short) 0xffff, message);
        host.send(frame);
    }
}
