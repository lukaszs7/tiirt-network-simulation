package com.tirt.network;

import java.util.Arrays;

/**
 * Created by Szab on 2017-04-09.
 */
public class EthernetFrame {
    private MacAddress recipient;
    private MacAddress sender;
    private short type;
    private byte[] data;

    public static final int GLOBAL_MTU = 1500;

    public EthernetFrame(MacAddress recipient, MacAddress sender, short type, byte[] data) {
        this.recipient = recipient;
        this.sender = sender;
        this.type = type;
        this.data = data;
    }

    public EthernetFrame(MacAddress recipient, MacAddress sender, short type, String data) {
        this.recipient = recipient;
        this.sender = sender;
        this.type = type;
        this.data = data.getBytes();
    }

    public MacAddress getRecipient() {
        return this.recipient;
    }

    public void setRecipient(byte[] macAddress) {
        this.recipient = new MacAddress(macAddress);
    }

    public void setRecipient(MacAddress recipient) {
        if (recipient != null) {
            this.recipient = recipient;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public MacAddress getSender() {
        return this.sender;
    }

    public void setSender(byte[] macAddress) {
        this.sender = new MacAddress(macAddress);
    }

    public void setSender(MacAddress sender) {
        if (sender != null) {
            this.sender = sender;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public short getType() {
        return this.type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public byte[] getData() {
        return this.data;
    }

    public void setData(byte[] data) {
        if (data != null) {
            this.data = data;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public void setData(String data) {
        if (data != null) {
            this.data = data.getBytes();
        } else {
            throw new IllegalArgumentException();
        }
    }

    public Boolean isBroadcast() {
        return this.checkMacAddress(this.recipient, (byte)255);
    }

    public Boolean isNull() {
        return this.checkMacAddress(this.recipient, (byte)0);
    }

    private Boolean checkMacAddress(MacAddress mac, byte value) {
        byte[] bytes = recipient.getAddress();
        Boolean result = true;

        for (int i = 0; i < bytes.length && result; i++) {
            result = result && bytes[i] == value;
        }

        return result;
    }
}
