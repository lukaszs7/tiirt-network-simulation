package com.tirt.utils;

import java.awt.*;
import java.util.Map;

/**
 * Created by Szab on 2017-04-09.
 */
public interface ActionTakenListener {
    void onActionTaken(EventType eventType, Map<String, Object> parameters);
}
