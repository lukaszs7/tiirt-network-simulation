package com.tirt.utils;

import com.tirt.network.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.*;

/**
 * Created by Łukasz on 06.04.2017.
 */

public class XMLLoader {

    private static final String[] INTERFACE_NODES = {"MacAddress", "IpAddress", "Netmask"};
    private static final String[] ROUTING_TABLE_NODES = {"Destination", "Netmask", "Gateway"};
    private static final String INTERVAL_IP = "ip";
    private static final String MARKOV_GENERATOR = "markov";
    private static final String BURST_GENERATOR = "burst";

    public Network parseXmlFileToNetwork(String xmlFile) {
        Document sourceDoc = getXmlDocument(xmlFile);
        List<Element> devicesElements = parseToElementsByName(sourceDoc, "Devices");
        List<Device> devices = createDevicesFromElements(devicesElements);
        List<Element> connectionsElements = parseToElementsByName(sourceDoc, "Connections");
        Network network = new Network(devices);
        List<SteadyGenerator> generators = createGenerators(network, devicesElements);
        network.setGenerators(generators);
        createConnectionsFromElements(connectionsElements, devices, network);
        return network;
    }

    private Document getXmlDocument(String xmlFile) {
        File fXmlFile = new File(xmlFile);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            return doc;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<SteadyGenerator> createGenerators(Network network, List<Element> devicesElements) {
        List<SteadyGenerator> generators = new ArrayList<>();
        List<Device> devices = network.getDevices();
        for (int i = 0; i < devices.size(); i++) {
            Device device = devices.get(i);
            Element element = devicesElements.get(i);
            String intervalStr = getTextContent(element, "Interval", 0);
            int interval = intervalStr == null ? 0 : Integer.parseInt(intervalStr);
            Node intervalNode = getElementAsNode(element, "Interval", 0);
            String type = getNodeAttribute(intervalNode, "type");
            String generator = getNodeAttribute(intervalNode, "generator");
            device.setInterval(interval);
            if(interval != 0) {
                if(MARKOV_GENERATOR.equals(generator)) {
                    if (type != null && type.equals(INTERVAL_IP)) {

                    } else {

                    }
                } else if (BURST_GENERATOR.equals(generator)) {
                    if (type != null && type.equals(INTERVAL_IP)) {

                    } else {

                    }
                } else {
                    if (type != null && type.equals(INTERVAL_IP)) {
                        IpHost ipHost = (IpHost) device;
                        generators.add(new SteadyPacketGenerator(ipHost, network, ipHost.getInterval()));
                    } else {
                        generators.add(new SteadyFrameGenerator(device, network, device.getInterval()));
                    }
                }
            }
        }
        return generators;
    }

    private List<Element> parseToElementsByName(Document sourceDoc, String nodeName) {
        List<Element> elements = new ArrayList<>();
        NodeList nList = sourceDoc.getElementsByTagName(nodeName);
        Node devices = nList.item(0);
        NodeList devicesNodes = devices.getChildNodes();
        for (int i = 0; i < devicesNodes.getLength(); i++) {
            Node deviceNode = devicesNodes.item(i);
            if (deviceNode.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) deviceNode;
                elements.add(element);
            }
        }
        return elements;
    }

    private List<Device> createDevicesFromElements(List<Element> elements) {
        List<Device> devices = new ArrayList<>();
        for (int i = 0; i < elements.size(); i++) {
            Element element = elements.get(i);
            Device device = createProperDevice(element);
            devices.add(device);
        }
        return devices;
    }

    private void createConnectionsFromElements(List<Element> elements, List<Device> devices, Network network) {
        for (int i = 0; i < elements.size(); i++) {
            Element element = elements.get(i);
            createProperConnection(element, devices, network);
        }
    }

    private void createProperConnection(Element element, List<Device> devices, Network network) {
        Node node1 = getElementAsNode(element, "Device", 0);
        String macAddress1 = getNodeAttribute(node1, "MacAddress");
        Node node2 = getElementAsNode(element, "Device", 1);
        String macAddress2 = getNodeAttribute(node2, "MacAddress");

        //network.connectDevicesByMacs(new MacAddress(macAddress1), new MacAddress(macAddress2));
        Device device1 = Network.findDeviceByMac(devices, new MacAddress(macAddress1));
        Device device2 = Network.findDeviceByMac(devices, new MacAddress(macAddress2));

        device1.connectDevice(device2);
        //device2.connectDevice(device1);
    }

    private Device createProperDevice(Element sourceDevice) {
        Device device = null;
        String type = sourceDevice.getNodeName();
        Element interfacesElem = childElement(sourceDevice, "Interfaces");
        List<Map<String, String>> interfacesProps = parseNodeWithChilds(interfacesElem, "Interface", INTERFACE_NODES);
        String queueCapacity = getTextContent(sourceDevice, "QueueCapacity", 0);
        String intervalStr = getTextContent(sourceDevice, "Interval", 0);
        String defaultGateway = getTextContent(sourceDevice, "DefaultGateway", 0);
        Map<Integer, MacAddress> ownMacs = createInterfaces(interfacesProps);

        switch (type) {
            case "Router":
                Element routingTableElemRouter = childElement(sourceDevice, "RoutingTable");
                List<Map<String, String>> routingTablePropsRouter = parseNodeWithChilds(routingTableElemRouter, "Entry", ROUTING_TABLE_NODES);
                int interval = intervalStr != null ? Integer.parseInt(intervalStr) : 0;
                device = new Router(ownMacs, Integer.valueOf(queueCapacity), new IpAddr(defaultGateway));
                fillIpHost((Router) device, interfacesProps, routingTablePropsRouter, interval);
                break;
            case "Switch":
                device = new Switch(ownMacs, Integer.valueOf(queueCapacity));
                break;
            case "Host":
                Element routingTableElem = childElement(sourceDevice, "RoutingTable");
                List<Map<String, String>> routingTableProps = parseNodeWithChilds(routingTableElem, "Entry", ROUTING_TABLE_NODES);
                int intervalHost = intervalStr != null ? Integer.parseInt(intervalStr) : 0;
                device = new IpHost(ownMacs, Integer.valueOf(queueCapacity), new IpAddr(defaultGateway));
                fillIpHost((IpHost) device, interfacesProps, routingTableProps, intervalHost);
                break;
        }
        return device;
    }

    private Device fillIpHost(IpHost ipHost, List<Map<String, String>> interfacesProps, List<Map<String, String>> routingTableProps, int interval) {
        String[] ipAddr = new String[interfacesProps.size()];
        String[] mac = new String[interfacesProps.size()];

        for (int i = 0; i < interfacesProps.size(); i++) {
            ipAddr[i] = interfacesProps.get(i).get(INTERFACE_NODES[1]);
            mac[i] = interfacesProps.get(i).get(INTERFACE_NODES[0]);
        }
        ipHost.mergeRoutingTable(createRoutingTable(routingTableProps));
        HashMap<MacAddress, IpAddr> ownIps = new HashMap();
        for (int i = 0; i < interfacesProps.size(); i++) {
            ownIps.put(new MacAddress(mac[i]), new IpAddr(ipAddr[i]));
        }
        ipHost.setOwnIps(ownIps);
        ipHost.setInterval(interval);
        return ipHost;
    }

    private Map<Integer, MacAddress> createInterfaces(List<Map<String, String>> source) {
        Map<Integer, MacAddress> ownMacs = new HashMap<>();
        for (int i = 0; i < source.size(); i++) {
            ownMacs.put(i, new MacAddress(source.get(i).get(INTERFACE_NODES[0])));
        }
        return ownMacs;
    }

    private TreeMap<SubnetAddr, IpAddr> createRoutingTable(List<Map<String, String>> source) {
        TreeMap<SubnetAddr, IpAddr> routingTable = new TreeMap<>();
        for (int i = 0; i < source.size(); i++) {
            Map<String, String> row = source.get(i);
            String destination = row.get(ROUTING_TABLE_NODES[0]);
            String netmask = row.get(ROUTING_TABLE_NODES[1]);
            String gateway = row.get(ROUTING_TABLE_NODES[2]);
            routingTable.put(new SubnetAddr(new IpAddr(destination), netmask), new IpAddr(gateway));
        }
        return routingTable;
    }

    private List<Map<String, String>> parseNodeWithChilds(Element sourceDevice, String rootNode, String[] childNodes) {
        NodeList childs = sourceDevice.getElementsByTagName(rootNode);
        List<Map<String, String>> listNodes = new ArrayList<>(childs.getLength());
        Map<String, String> interfaceProps;

        for (int i = 0; i < childs.getLength(); i++) {
            Node interfaceNode = childs.item(i);
            interfaceProps = new HashMap<>();
            for (int j = 0; j < childNodes.length; j++) {
                if (interfaceNode.getNodeType() == Node.ELEMENT_NODE) {
                    String textContent = getTextContent((Element) interfaceNode, childNodes[j], 0);
                    interfaceProps.put(childNodes[j], textContent);
                }
            }
            listNodes.add(interfaceProps);
        }
        return listNodes;
    }

    private String getTextContent(Element root, String nodeName, int index) {
        Node node = root.getElementsByTagName(nodeName).item(index);
        return node != null ? node.getTextContent() : null;
    }

    private Node getElementAsNode(Element root, String nodeName, int index) {
        return root.getElementsByTagName(nodeName).item(index);
    }

    private String getNodeAttribute(Node node, String key) {
        if (node != null) {
            Node item = node.getAttributes().getNamedItem(key);
            return item != null ? item.getNodeValue() : null;
        } else {
            return null;
        }
    }

    private Element childElement(Element rootNode, String name) {
        return (Element) rootNode.getElementsByTagName(name).item(0);
    }
}
