package com.tirt.utils;

import com.tirt.network.Device;
import com.tirt.network.EthernetFrame;
import com.tirt.network.MacAddress;

import java.util.*;

/**
 * Created by Szab on 2017-04-27.
 */
public class StatsManager implements ActionTakenListener {
    private static StatsManager instance = new StatsManager();

    public boolean log = false;

    public class OutgoingPacketsStatistics {
        public int sent = 0;
        public int dropped = 0;
        public int received = 0;
    }

    public class InternalStatistics {
        public int sent = 0;
        public int received = 0;
        public int dropped = 0;
    }

    public class Statistics {
        public OutgoingPacketsStatistics outgoingStatistics = new OutgoingPacketsStatistics();
        public InternalStatistics internalStatistics = new InternalStatistics();
    }

    private HashMap<Object, Statistics> localStatistics = new HashMap<>();

    public Statistics getStatisticForDevice(Object device) {
        return localStatistics.get(device);
    }

    public Statistics getGlobalStatistic() {
        Statistics result = new Statistics();

        localStatistics.forEach((o, statistic) -> {
            result.internalStatistics.sent += statistic.internalStatistics.sent;
            result.internalStatistics.received += statistic.internalStatistics.received;
            result.internalStatistics.dropped += statistic.internalStatistics.dropped;

            result.outgoingStatistics.sent += statistic.outgoingStatistics.sent;
            result.outgoingStatistics.received += statistic.outgoingStatistics.received;
            result.outgoingStatistics.dropped += statistic.outgoingStatistics.dropped;
        });

        return result;
    }

    public Statistics getStatisticByMac(MacAddress macAddress) {
        for(Object key : localStatistics.keySet()) {
            Device device = (Device)key;

            if (device != null && device.hasMacAddress(macAddress)) {
                return localStatistics.get(device);
            }
        }

        return new Statistics();
    }

    private void onFrameDropped(Device device, EthernetFrame frame) {
        Statistics localStats = getStatisticByMac(frame.getSender());
        localStats.outgoingStatistics.dropped++;
    }

    private void onFrameSent(Device device, EthernetFrame frame) {
        if (device.hasMacAddress(frame.getSender())) {
            Statistics localStats = getStatisticByMac(frame.getSender());
            localStats.outgoingStatistics.sent++;
        }
    }

    private void onFrameReceived(Device device, EthernetFrame frame) {
        if (device.hasMacAddress(frame.getRecipient())) {
            Statistics localStats = getStatisticByMac(frame.getSender());
            localStats.outgoingStatistics.received++;
        }
    }

    private static String pre(Device sender) {
        return "[" + sender.getClass().getSimpleName() + " " + sender.name + "] ";
    }

    private void onFrameDroppedExplicitly(Device s, EthernetFrame frame) {
        if (log)
            System.out.println(pre(s) + "explicit frame drop");
    }

    private void onMacTableUpdated(Device s, MacAddress newAddr, int newIface) {
        if (log)
            System.out.println(pre(s) + "mac table updated: (" + newAddr + ", " + newIface + ")");
    }

    private void onMacTableEntryOverwritten(Device s, MacAddress addr, int newIface, int oldIface) {
        if (log)
            System.out.println(pre(s) + "mac table entry overwritten for " + addr + " (" + oldIface + "->" + newIface + ")");
    }

    @Override
    public void onActionTaken(EventType eventType, Map<String, Object> parameters) {
        synchronized (this) {
            Device sender = (Device)parameters.get("sender");
            EthernetFrame frame = (EthernetFrame)parameters.get("frame");

            if (!localStatistics.containsKey(sender)) {
                localStatistics.put(sender, new Statistics());
            }

            Statistics stat = localStatistics.get(sender);

            switch(eventType) {
                case FRAME_SENT:
                    stat.internalStatistics.sent++;
                    onFrameSent(sender, frame);
                    break;
                case FRAME_RECEIVED:
                    stat.internalStatistics.received++;
                    onFrameReceived(sender, frame);
                    break;
                case FRAME_DROPPED:
                    stat.internalStatistics.dropped++;
                    onFrameDropped(sender, frame);
                    break;
                case FRAME_DROPPED_EXPLICITLY:
                    onFrameDroppedExplicitly(sender, frame);
                    break;
                case MAC_TABLE_ENTRY_OVERWRITTEN:
                    onMacTableEntryOverwritten(sender,
                            (MacAddress) parameters.get("addr"),
                            (Integer) parameters.get("new-iface"),
                            (Integer) parameters.get("old-iface"));
                    break;
                case MAC_TABLE_UPDATED:
                    onMacTableUpdated(sender,
                            (MacAddress) parameters.get("addr"),
                            (Integer) parameters.get("iface"));
                    break;
            }
        }
    }

    @Override
    public String toString() {
        Statistics global = getGlobalStatistic();

        StringBuilder builder = new StringBuilder();
        builder.append("Global statistics:\n");
        builder.append("Sent: " + global.internalStatistics.sent + "\n");
        builder.append("Dropped: " + global.internalStatistics.dropped + "\n");
        builder.append("Received: " + global.internalStatistics.received + "\n");


        return builder.toString();
    }

    public static StatsManager getInstance() {
        return instance;
    }

    private StatsManager()
    {

    }
}
