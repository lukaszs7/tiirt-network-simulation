package com.tirt.config;
import com.tirt.network.*;
import com.tirt.ui.Menu;
import com.tirt.ui.MenuNetworkTraffic;
import com.tirt.utils.ActionTakenListener;
import com.tirt.utils.StatsManager;
import com.tirt.utils.XMLLoader;
import java.util.*;

/**
 * Created by Łukasz on 12.03.2017.
 */

public class App {
    private static final String NETWORK_1 = "src/main/resources/network.xml";
    private static final String NETWORK_2 = "src/main/resources/network-simple.xml";
    private static final String NETWORK_3 = "src/main/resources/network-cycles.xml";
    private static final String NETWORK_4 = "src/main/resources/network-arpload.xml";

    private String[] args;

    public App(String[] args) {
        this.args = args;
    }

    private void run() {
        Network net = loadXMLFile(args.length > 0 ? args[0] : NETWORK_1);
        Menu menu = new Menu(net);
        Thread th = new Thread(menu);
        net.go();
        th.start();
        net.block();
    }

    private Map<Integer, MacAddress> createInterfaces(int numOfInterfaces) {
        Map<Integer, MacAddress> ownMacs = new HashMap<>();
        for (int i = 0; i < numOfInterfaces; i++) {
            ownMacs.put(i, new MacAddress());
        }
        return ownMacs;
    }

    public Network loadXMLFile(String netPath) {
        XMLLoader xmlLoader = new XMLLoader();
        Network network = xmlLoader.parseXmlFileToNetwork(netPath);
        return network;
    }

    public static void main(String[] args) {
        App app = new App(args);
        app.run();
    }
}
